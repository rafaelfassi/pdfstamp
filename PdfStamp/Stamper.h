#ifndef STAMPER_H
#define STAMPER_H

#include "PrintData.h"

class CStamperPrivate;

class CStamper {
public:
    CStamper(const std::string &srcDoc, const std::string &destDoc, std::vector<PrintData> &vData);
    ~CStamper();
    bool stampDocument();

private:
    CStamperPrivate *p;
};

#endif // STAMPER_H
