#include "Stamper.h"
#include "Stamper_p.h"

#include "PDFWriter.h"
#include "PDFImageXObject.h"
#include "PDFPage.h"
#include "PDFPageInput.h"
#include "PageContentContext.h"
#include "PDFFormXObject.h"
#include "PDFModifiedPage.h"
#include "AbstractContentContext.h"
#include "PDFDocumentCopyingContext.h"
#include "DictionaryContext.h"
#include "PDFUsedFont.h"

#include <iostream>
#include <fstream>

CStamperPrivate::CStamperPrivate(const std::string &srcDoc, const std::string &destDoc, std::vector<PrintData> &vData) :
    m_srcDoc(srcDoc), m_destDoc(destDoc), m_vData(vData), m_pdfWriter(0), m_gsId(0), m_gsIdS(0), m_pageCount(0)
{
    m_pdfWriter = new PDFWriter();
}

CStamperPrivate::~CStamperPrivate()
{
    delete m_pdfWriter;
}

bool CStamperPrivate::stampDocument()
{
    PDFParser pdfParser;
    InputFile pdfFile;
    EStatusCode status;

    m_vDataBuf.clear();
    for(size_t i = 0; i < m_vData.size(); ++i)
        m_vDataBuf.push_back(PrintDataBuf());

    status = m_pdfWriter->StartPDF(m_destDoc, ePDFVersion13);
    if(status != PDFHummus::eSuccess)
    {
        std::cout << "failed to start PDFWriter for: " << m_destDoc << std::endl;
        return false;
    }

    status = pdfFile.OpenFile(m_srcDoc);
    if(status != PDFHummus::eSuccess)
    {
        std::cout << "unable to open source file for: " << m_srcDoc << std::endl;
        return false;
    }

    status = pdfParser.StartPDFParsing(pdfFile.GetInputStream());
    if(status != PDFHummus::eSuccess)
    {
        std::cout << "failed to start PDFParser\n";
        return false;
    }

    PDFDocumentCopyingContext *cpyCxt = m_pdfWriter->CreatePDFCopyingContext(m_srcDoc);

    // Trasparencia
    ObjectsContext &objGsCxt = m_pdfWriter->GetObjectsContext();
    m_gsId = objGsCxt.StartNewIndirectObject();
    DictionaryContext *dictGs = objGsCxt.StartDictionary();
    dictGs->WriteKey("type");
    dictGs->WriteNameValue("ExtGState");
    dictGs->WriteKey("ca");
    objGsCxt.WriteDouble(0.5); // opacidade
    objGsCxt.EndLine();
    objGsCxt.EndDictionary(dictGs);

    // Trasparencia Stroking
    ObjectsContext &objGsCxtS = m_pdfWriter->GetObjectsContext();
    m_gsIdS = objGsCxt.StartNewIndirectObject();
    DictionaryContext *dictGsS = objGsCxt.StartDictionary();
    dictGsS->WriteKey("type");
    dictGsS->WriteNameValue("ExtGState");
    dictGsS->WriteKey("CA");
    objGsCxtS.WriteDouble(0.5); // opacidade
    objGsCxtS.EndLine();
    objGsCxtS.EndDictionary(dictGsS);

    m_pageCount = pdfParser.GetPagesCount();

    for (unsigned long i = 0; i < m_pageCount; ++i)
    {
        EStatusCodeAndObjectIDType cpyCxtRes = cpyCxt->CreateFormXObjectFromPDFPage(i, ePDFPageBoxMediaBox);
        if(cpyCxtRes.first != PDFHummus::eSuccess)
        {
            status = PDFHummus::eFailure;
            return false;
        }

        ObjectIDType formObjectId = cpyCxtRes.second;
        PDFDictionary *dictionary = pdfParser.ParsePage(i);
        PDFPageInput pageInput(&pdfParser, dictionary);

        PDFPage* page = new PDFPage();
        page->SetMediaBox(pageInput.GetMediaBox());
        PageContentContext* pageContentContext = m_pdfWriter->StartPageContentContext(page);
        if(NULL == pageContentContext)
        {
            status = PDFHummus::eFailure;
            std::cout<<"failed to create content context for page\n";
            return false;
        }

        if(!addPageData(page, pageContentContext, &pageInput, i, false))
            return false;

        pageContentContext->q();
        pageContentContext->cm(1,0,0,1,0,0);
        pageContentContext->Do(page->GetResourcesDictionary().AddFormXObjectMapping(formObjectId));
        pageContentContext->Q();

        if(!addPageData(page, pageContentContext, &pageInput, i, true))
            return false;

        status = m_pdfWriter->EndPageContentContext(pageContentContext);
        if(status != PDFHummus::eSuccess)
        {
            std::cout<<"failed to end page content context\n";
            return false;
        }

        status = m_pdfWriter->WritePageAndRelease(page);
        if(status != PDFHummus::eSuccess)
        {
            std::cout<<"failed to write page\n";
            return false;
        }
    }

    status = m_pdfWriter->EndPDF();
    if(status != PDFHummus::eSuccess)
    {
        std::cout<<"failed to end pdf\n";
        return false;
    }

    return true;
}

bool CStamperPrivate::addPageData(PDFPage* page, PageContentContext *pageContentContext, PDFPageInput *pageInput,
                 unsigned long pageIdx, bool topLayer)
{
    PDFRectangle dimensions = pageInput->GetMediaBox();

    for(size_t i = 0; i < m_vData.size(); ++i)
    {
        PrintData &dto = m_vData.at(i);

        if(dto.topLayer != topLayer)
            continue;

        if(!((dto.pageNum == pageIdx+1)
             || (dto.pages & PDP_FIRST && pageIdx == 0)
             || (dto.pages & PDP_LAST && pageIdx == m_pageCount-1)
             || (dto.pages & PDP_MIDDLE && pageIdx > 0 && pageIdx < m_pageCount-1)
             || (dto.pages & PDP_ODD && pageIdx%2 == 0)
             || (dto.pages & PDP_EVEN && pageIdx%2 != 0)))
            continue;

        PDFRectangle parentDim;

        if(dto.parent.empty())
        {
            parentDim = dimensions;
        }
        else
        {
            for(size_t x = 0; x < m_vDataBuf.size(); ++x)
            {
                PrintDataBuf &parent = m_vDataBuf.at(x);
                if(parent.name == dto.parent)
                {
                    parentDim.LowerLeftX = parent.posX;
                    parentDim.LowerLeftY = parent.posY;
                    parentDim.UpperRightX = parent.width;
                    parentDim.UpperRightY = parent.height;
                    break;
                }
            }
        }

        PrintDataBuf &dt = m_vDataBuf.at(i);
        if(dt.pageDim != parentDim)
        {
            dt.reset(m_vData.at(i), parentDim);

            if(dt.type == PDT_TEXT && !dt.fontPtr)
            {
                std::ifstream f(dt.font.c_str());
                if(!f.good())
                {
                    std::cout << "Failed to open font file for: " << dt.font << std::endl;
                    return false;
                }

                dt.fontPtr = m_pdfWriter->GetFontForFile(dt.font);
                if(!dt.fontPtr)
                {
                    std::cout << "Failed to create font for: " << dt.font << std::endl;
                    return false;
                }
            }
            else if(dt.type == PDT_IMG)
            {
                std::ifstream f(dt.data.c_str());
                if(!f.good())
                {
                    std::cout << "Failed to open image file for: " << dt.data << std::endl;
                    return false;
                }
            }

            if(dt.width < 0.0 || dt.height < 0.0)
            {
                if(dt.type == PDT_IMG)
                {
                    DoubleAndDoublePair imgDimensions = m_pdfWriter->GetImageDimensions(dt.data);
                    if(dt.width < 0.0)
                        dt.width = imgDimensions.first;
                    if(dt.height < 0.0)
                        dt.height = imgDimensions.second;
                }
                else if(dt.type == PDT_TEXT && dt.fontPtr)
                {
                    PDFUsedFont::TextMeasures textDimensions = dt.fontPtr->CalculateTextDimensions(dt.data, dt.fontSize);
                    if(dt.width < 0.0)
                        dt.width = textDimensions.width;
                    if(dt.height < 0.0)
                        dt.height = textDimensions.height;
                }
            }

            if(dt.align & PDA_H_EXPAND)
                dt.width = parentDim.UpperRightX - dt.leftMargin - dt.rightMargin - dt.borderWidth;

            if(dt.align & PDA_V_EXPAND)
                dt.height = parentDim.UpperRightY - dt.topMargin - dt.bottomMargin - dt.borderWidth;

            if(dt.type == PDT_RECT && !dt.fill)
            {
                dt.width += dt.borderWidth;
                dt.height += dt.borderWidth;
                dt.leftMargin += dt.borderWidth/2;
                dt.rightMargin -= dt.borderWidth/2;
                dt.topMargin -= dt.borderWidth/2;
                dt.bottomMargin += dt.borderWidth/2;
            }

            if(dt.rotationDeg)
            {
                dt.rotator.process(dt.rotationDeg, dt.width, dt.height);
                dt.shapeWidth = dt.rotator.width;
                dt.shapeHeight = dt.rotator.height;
            }
            else
            {
                dt.shapeWidth = dt.width;
                dt.shapeHeight = dt.height;
            }

            if(dt.posX < 0.0)
            {
                if(dt.align & PDA_H_LEFT || dt.align & PDA_H_EXPAND)
                    dt.posX = parentDim.LowerLeftX + dt.leftMargin;
                else if(dt.align & PDA_H_RIGHT)
                    dt.posX = parentDim.LowerLeftX + parentDim.UpperRightX - dt.shapeWidth - dt.rightMargin;
                else //if(dt.align & PDA_H_CENTER)
                    dt.posX = parentDim.LowerLeftX + parentDim.UpperRightX/2 - dt.shapeWidth/2 + dt.leftMargin - dt.rightMargin;
            }

            if(dt.posY < 0.0)
            {
                if(dt.align & PDA_V_TOP)
                    dt.posY = parentDim.LowerLeftY + parentDim.UpperRightY - dt.shapeHeight - dt.topMargin;
                else if(dt.align & PDA_V_BOTTOM || dt.align & PDA_V_EXPAND)
                    dt.posY = parentDim.LowerLeftY + dt.bottomMargin;
                else //if(dt.align & PDA_V_CENTER)
                    dt.posY = parentDim.LowerLeftY + parentDim.UpperRightY/2 - dt.shapeHeight/2 + dt.bottomMargin - dt.topMargin;
            }

            // If there was rotation, it is necessary do adjust tha position
            dt.posX += dt.rotator.offsetX;
            dt.posY += dt.rotator.offsetY;
        }

        if(dt.type == PDT_IMG)
        {
            AbstractContentContext::ImageOptions imgOpt;
            imgOpt.transformationMethod = AbstractContentContext::eFit;
            imgOpt.boundingBoxHeight = dt.height;
            imgOpt.boundingBoxWidth = dt.width;

            pageContentContext->q();
            dt.rotator.rotate(pageContentContext, dt.posX, dt.posY);
            pageContentContext->DrawImage(0, 0, dt.data, imgOpt);
            pageContentContext->Q();
        }
        else if(dt.type == PDT_TEXT && dt.fontPtr)
        {
            AbstractContentContext::TextOptions textOptions(dt.fontPtr);
            textOptions.fontSize = dt.fontSize;
            textOptions.colorSpace = AbstractContentContext::eRGB;
            textOptions.colorValue = dt.color;

            pageContentContext->q();
            if(dt.transparent)
                pageContentContext->gs(getGstTansparency(page, GTT_TEXT_AND_FILL));
            dt.rotator.rotate(pageContentContext, dt.posX, dt.posY);
            pageContentContext->WriteText(0, 0, dt.data, textOptions);
            pageContentContext->Q();
        }
        else if(dt.type == PDT_RECT)
        {
            AbstractContentContext::GraphicOptions pathFillOptions(dt.fill ? AbstractContentContext::eFill : AbstractContentContext::eStroke,
                                                                   AbstractContentContext::eRGB,
                                                                   dt.color,
                                                                   dt.borderWidth);
            pageContentContext->q();
            if(dt.transparent)
                pageContentContext->gs(getGstTansparency(page, dt.fill ? GTT_TEXT_AND_FILL : GTT_STROKE));
            dt.rotator.rotate(pageContentContext, dt.posX, dt.posY);
            pageContentContext->DrawRectangle(0, 0, dt.width - dt.borderWidth, dt.height - dt.borderWidth, pathFillOptions);
            pageContentContext->Q();
        }
    }

    return true;
}

std::string CStamperPrivate::getGstTansparency(PDFPage *page, GsTansparencyType type)
{
    return page->GetResourcesDictionary().AddExtGStateMapping(type == GTT_STROKE ? m_gsIdS : m_gsId);
}

// ***************** CStamper *****************

CStamper::CStamper(const std::string &srcDoc, const std::string &destDoc, std::vector<PrintData> &vData)
{
    p = new CStamperPrivate(srcDoc, destDoc, vData);
}

CStamper::~CStamper()
{
    delete p;
}

bool CStamper::stampDocument()
{
    return p->stampDocument();
}

