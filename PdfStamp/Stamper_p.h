#ifndef STAMPERPRIVATE_H
#define STAMPERPRIVATE_H

#include <string>
#include <vector>
#include "PrintData.h"
#include "Rotator.h"
#include "PDFRectangle.h"

class PDFUsedFont;
class PDFWriter;
class PDFPageInput;
class PDFPage;

enum GsTansparencyType {
    GTT_TEXT_AND_FILL,
    GTT_STROKE
};

class PrintDataBuf : public PrintData {
public:
    void reset(PrintData &dt, PDFRectangle &dim)
    {
        PrintData &dtR = *this;
        dtR = dt;
        pageDim = dim;
        shapeWidth = width;
        shapeHeight = height;
        rotator = Rotator();
    }

    PrintDataBuf() : fontPtr(0), shapeWidth(0.0), shapeHeight(0.0) {}
    PDFRectangle pageDim;
    Rotator rotator;
    PDFUsedFont *fontPtr;
    double shapeWidth;
    double shapeHeight;
};

class CStamperPrivate {
public:
    CStamperPrivate(const std::string &srcDoc, const std::string &destDoc, std::vector<PrintData> &vData);
    ~CStamperPrivate();
    bool stampDocument();
    bool addPageData(PDFPage* page, PageContentContext *pageContentContext, PDFPageInput *pageInput,
                     unsigned long pageIdx, bool topLayer);
    std::string getGstTansparency(PDFPage *page, GsTansparencyType type);

    std::string m_srcDoc;
    std::string m_destDoc;
    std::vector<PrintData> &m_vData;
    std::vector<PrintDataBuf> m_vDataBuf;
    PDFWriter *m_pdfWriter;
    unsigned long m_gsId;
    unsigned long m_gsIdS;
    unsigned long m_pageCount;
};

#endif // STAMPERPRIVATE_H
