#ifndef ROTATOR_H
#define ROTATOR_H

class PageContentContext;

class Rotator {
public:
    double offsetX;
    double offsetY;
    double width;
    double height;
    double cosVal;
    double sinVal;

    Rotator();
    void process(double rotation, double orWidth, double orHeight);
    void rotate(PageContentContext* pageContentContext, double posX, double posY);
};

#endif // ROTATOR_H
