#ifndef PRINTDATA_H
#define PRINTDATA_H

#include <string>
#include <vector>

enum PrintDataType {
    PDT_TEXT,
    PDT_IMG,
    PDT_RECT
};

enum PrintDataPages {
    PDP_FIRST = 0x01,
    PDP_MIDDLE = 0x02,
    PDP_LAST = 0x04,
    PDP_ODD = 0x08,
    PDP_EVEN = 0x10
};

enum PrintDataAlignment {
    PDA_H_LEFT = 0x01,
    PDA_H_RIGHT = 0x02,
    PDA_H_CENTER = 0x04,
    PDA_H_EXPAND = 0x08,
    PDA_V_TOP = 0x10,
    PDA_V_BOTTOM = 0x20,
    PDA_V_CENTER = 0x40,
    PDA_V_EXPAND = 0x80
};

typedef std::pair<std::string,std::string> TagType;

class PrintData {
public:
    PrintData();
    static bool loadListFormXmlFile(std::vector<PrintData> &vdt, const std::string &xmlFile,
                                    std::vector<TagType> &vtg = std::vector<TagType>());
    static bool loadListFormXml(std::vector<PrintData> &vdt, const std::string &xml);
    static bool saveXmlFileSample(const std::string &xmlFile);

    int type;
    unsigned long pageNum;
    int pages;
    bool topLayer;
    bool transparent;
    bool fill;
    double posX;
    double posY;
    double width;
    double height;
    double leftMargin;
    double rightMargin;
    double topMargin;
    double bottomMargin;
    double rotationDeg;
    double borderWidth;
    int align;
    long fontSize;
    unsigned long color;
    std::string name;
    std::string parent;
    std::string data;
    std::string font;
};

#endif // PRINTDATA_H
