#include <windows.h>
#include <iostream>

using namespace std;

typedef int (__stdcall *f_CreateNewTagList)();
typedef bool (__stdcall *f_AddTag)(int tagLstNum, const char *key, const char *value);
typedef bool (__stdcall *f_StampPDFFromXmlFile)(const char *src, const char *out, const char *xmlFile, int tagLstNum);
typedef bool (__stdcall *f_StampPDFFromXml)(const char *src, const char *out, const char *xml);

const char sXml[] = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                  "<PrintDataList>"
                      "<PrintData>"
                          "<!--Tipo de objeto a ser desenhado: TEXT=Texto; IMG=Imagem; RECT=Retangulo-->"
                          "<type>TEXT</type>"
                          "<!--Desenha o item em uma pagina espacifica-->"
                          "<pageNum>0</pageNum>"
                          "<!--Indica se o item deve ser desenhado na primeira pagina-->"
                          "<firstPage>true</firstPage>"
                          "<!--Indica se o item deve ser desenhado nas paginas entre a primeira e a ultima-->"
                          "<internalPages>true</internalPages>"
                          "<!--Indica se o item deve ser desenhado na ultima pagina-->"
                          "<lastPage>true</lastPage>"
                          "<!--Desenha o item sobre do conteudo original da pagina-->"
                          "<topLayer>true</topLayer>"
                          "<!--Desenha o item transparente. Nao funciona para imagens, nas quais a transparencia pode ser adicionada no proprio arquivo png-->"
                          "<transparent>false</transparent>"
                          "<!--Quando o desenho for do tipo retangulo, indica se o mesmo deve ser preenchido-->"
                          "<fill>true</fill>"
                          "<!--Tamanho da margem esquerda quando se usa alinhamento-->"
                          "<leftMargin>10</leftMargin>"
                          "<!--Tamanho da margem direita quando se usa alinhamento-->"
                          "<rightMargin>0</rightMargin>"
                          "<!--Tamanho da margem superior quando se usa alinhamento-->"
                          "<topMargin>10</topMargin>"
                          "<!--Tamanho da margem inferior quando se usa alinhamento-->"
                          "<bottomMargin>0</bottomMargin>"
                          "<!--Rotacao do item em graus-->"
                          "<rotationDeg>45.0</rotationDeg>"
                          "<!--Tamanho da borda para o elemento retangulo.-->"
                          "<borderWidth>1</borderWidth>"
                          "<!--Alinhamentos do item: H_LEFT|H_RIGHT|H_CENTER|H_EXPAND|V_TOP|V_BOTTOM|V_CENTER|V_EXPAND-->"
                          "<align>H_LEFT|V_TOP</align>"
                          "<!--Tamanho da fonte para texto-->"
                          "<fontSize>12</fontSize>"
                          "<!--Codigo GRB da cor para desenho de texto e retangulo-->"
                          "<color>000000</color>"
                          "<!--Nome do item. Usado para outro item referenciar como pai permitindo heranca-->"
                          "<name></name>"
                          "<!--Pai do item. Referencia o nome do objeto pai-->"
                          "<parent></parent>"
                          "<!--Quando o item for imagem este parametro indica o caminho da mesma e quando for texto indica o texto a ser desenhado-->"
                          "<data>[NOME]</data>"
                          "<!--Caminho para o arquivo de fonte a ser usado quanto o item for do tipo texto-->"
                          "<font>C:/Projetos/PDFStamp/tst/fonts/arial.ttf</font>"
                      "</PrintData>"
                      "<PrintData>"
                          "<!--Tipo de objeto a ser desenhado: TEXT=Texto; IMG=Imagem; RECT=Retangulo-->"
                          "<type>IMG</type>"
                          "<!--Desenha o item sobre do conteudo original da pagina-->"
                          "<topLayer>true</topLayer>"
                          "<!--Desenha o item transparente. Nao funciona para imagens, nas quais a transparencia pode ser adicionada no proprio arquivo png-->"
                          "<transparent>false</transparent>"
                          "<!--Quando o desenho for do tipo retangulo, indica se o mesmo deve ser preenchido-->"
                          "<fill>true</fill>"
                          "<!--Largura do item. Para imagem e texto este parametro e opcional-->"
                          "<width>100</width>"
                          "<!--Altura do item. Para imagem e texto este parametro e opcional-->"
                          "<height>50</height>"
                          "<!--Tamanho da margem esquerda quando se usa alinhamento-->"
                          "<leftMargin>10</leftMargin>"
                          "<!--Tamanho da margem direita quando se usa alinhamento-->"
                          "<rightMargin>0</rightMargin>"
                          "<!--Tamanho da margem superior quando se usa alinhamento-->"
                          "<topMargin>0</topMargin>"
                          "<!--Tamanho da margem inferior quando se usa alinhamento-->"
                          "<bottomMargin>10</bottomMargin>"
                          "<!--Rotacao do item em graus-->"
                          "<rotationDeg>0</rotationDeg>"
                          "<!--Tamanho da borda para o elemento retangulo.-->"
                          "<borderWidth>1</borderWidth>"
                          "<!--Alinhamentos do item: H_LEFT|H_RIGHT|H_CENTER|H_EXPAND|V_TOP|V_BOTTOM|V_CENTER|V_EXPAND-->"
                          "<align>H_CENTER|V_BOTTOM</align>"
                          "<!--Tamanho da fonte para texto-->"
                          "<fontSize>12</fontSize>"
                          "<!--Codigo GRB da cor para desenho de texto e retangulo-->"
                          "<color>000000</color>"
                          "<!--Nome do item. Usado para outro item referenciar como pai permitindo heranca-->"
                          "<name>ImgPai</name>"
                          "<!--Pai do item. Referencia o nome do objeto pai-->"
                          "<parent></parent>"
                          "<!--Quando o item for imagem este parametro indica o caminho da mesma e quando for texto indica o texto a ser desenhado-->"
                          "<data>C:/Projetos/PDFStamp/tst/img1.png</data>"
                      "</PrintData>"
                      "<PrintData>"
                          "<!--Tipo de objeto a ser desenhado: TEXT=Texto; IMG=Imagem; RECT=Retangulo-->"
                          "<type>TEXT</type>"
                          "<!--Desenha o item em uma pagina espacifica-->"
                          "<pageNum>0</pageNum>"
                          "<!--Indica se o item deve ser desenhado na primeira pagina-->"
                          "<firstPage>true</firstPage>"
                          "<!--Indica se o item deve ser desenhado nas paginas entre a primeira e a ultima-->"
                          "<internalPages>true</internalPages>"
                          "<!--Indica se o item deve ser desenhado na ultima pagina-->"
                          "<lastPage>true</lastPage>"
                          "<!--Desenha o item sobre do conteudo original da pagina-->"
                          "<topLayer>true</topLayer>"
                          "<!--Desenha o item transparente. Nao funciona para imagens, nas quais a transparencia pode ser adicionada no proprio arquivo png-->"
                          "<transparent>false</transparent>"
                          "<!--Quando o desenho for do tipo retangulo, indica se o mesmo deve ser preenchido-->"
                          "<fill>true</fill>"
                          "<!--Tamanho da margem esquerda quando se usa alinhamento-->"
                          "<leftMargin>10</leftMargin>"
                          "<!--Tamanho da margem direita quando se usa alinhamento-->"
                          "<rightMargin>0</rightMargin>"
                          "<!--Tamanho da margem superior quando se usa alinhamento-->"
                          "<topMargin>10</topMargin>"
                          "<!--Tamanho da margem inferior quando se usa alinhamento-->"
                          "<bottomMargin>0</bottomMargin>"
                          "<!--Rotacao do item em graus-->"
                          "<rotationDeg>0</rotationDeg>"
                          "<!--Tamanho da borda para o elemento retangulo.-->"
                          "<borderWidth>1</borderWidth>"
                          "<!--Alinhamentos do item: H_LEFT|H_RIGHT|H_CENTER|H_EXPAND|V_TOP|V_BOTTOM|V_CENTER|V_EXPAND-->"
                          "<align>H_CENTER|V_CENTER</align>"
                          "<!--Tamanho da fonte para texto-->"
                          "<fontSize>12</fontSize>"
                          "<!--Codigo GRB da cor para desenho de texto e retangulo-->"
                          "<color>0000FF</color>"
                          "<!--Nome do item. Usado para outro item referenciar como pai permitindo heranca-->"
                          "<name></name>"
                          "<!--Pai do item. Referencia o nome do objeto pai-->"
                          "<parent>ImgPai</parent>"
                          "<!--Quando o item for imagem este parametro indica o caminho da mesma e quando for texto indica o texto a ser desenhado-->"
                          "<data>I'm Lobão</data>"
                          "<!--Caminho para o arquivo de fonte a ser usado quanto o item for do tipo texto-->"
                          "<font>C:/Projetos/PDFStamp/tst/fonts/arial.ttf</font>"
                      "</PrintData>"
                      "<PrintData>"
                          "<type>IMG</type>"
                          "<fill>true</fill>"
                          "<leftMargin>10</leftMargin>"
                          "<topMargin>10</topMargin>"
                          "<rotationDeg>45</rotationDeg>"
                          "<borderWidth>1</borderWidth>"
                          "<align>H_CENTER|V_CENTER</align>"
                          "<name>ImgPai</name>"
                          "<data>C:/Projetos/PDFStamp/tst/img2.png</data>"
                      "</PrintData>"
                      "<PrintData>"
                          "<type>RECT</type>"
                          "<transparent>true</transparent>"
                          "<borderWidth>10</borderWidth>"
                          "<align>H_EXPAND|V_EXPAND</align>"
                          "<color>FF0000</color>"
                      "</PrintData>"
                  "</PrintDataList>";


int main(int argc, char *argv[])
{
    std::string src("C:\\Projetos\\PDFStamp\\tst\\TFVCvsGit.pdf");
    std::string out("C:\\Projetos\\PDFStamp\\tst\\Out.pdf");
    std::string xml("C:\\Projetos\\PDFStamp\\tst\\teste.xml");

    HINSTANCE hGetProcIDDLL = LoadLibrary("C:\\Projetos\\PDFStamp\\build-pdfstamp-Desktop_Qt_5_7_0_MSVC2015_64bit-Debug\\PdfStampApi\\PdfStampApi.dll");

    if (!hGetProcIDDLL)
    {
        std::cout << "could not load the dynamic library" << std::endl;
        return EXIT_FAILURE;
    }

    f_CreateNewTagList CreateNewTagList = (f_CreateNewTagList)GetProcAddress(hGetProcIDDLL, "CreateNewTagList");
    if (!CreateNewTagList)
    {
        std::cout << "could not locate the function CreateNewTagList" << std::endl;
        return EXIT_FAILURE;
    }

    f_AddTag AddTag = (f_AddTag)GetProcAddress(hGetProcIDDLL, "AddTag");
    if (!AddTag)
    {
        std::cout << "could not locate the function AddTag" << std::endl;
        return EXIT_FAILURE;
    }

    f_StampPDFFromXmlFile StampPDFFromXmlFile = (f_StampPDFFromXmlFile)GetProcAddress(hGetProcIDDLL, "StampPDFFromXmlFile");
    if (!StampPDFFromXmlFile)
    {
        std::cout << "could not locate the function StampPDFFromXmlFile" << std::endl;
        return EXIT_FAILURE;
    }

    f_StampPDFFromXml StampPDFFromXml = (f_StampPDFFromXml)GetProcAddress(hGetProcIDDLL, "StampPDFFromXml");
    if (!StampPDFFromXml)
    {
        std::cout << "could not locate the function StampPDFFromXml" << std::endl;
        return EXIT_FAILURE;
    }

    int num = CreateNewTagList();
    AddTag(num, "[NOME]", "Teste Lib");
    AddTag(num, "__TEXTO__", "Teste Lib 2");

    StampPDFFromXmlFile(src.c_str(), out.c_str(), xml.c_str(), num);
    //StampPDFFromXml(src.c_str(), out.c_str(), sXml);

    return EXIT_SUCCESS;
}
