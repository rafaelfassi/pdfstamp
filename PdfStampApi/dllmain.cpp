#include "Stamper.h"
#include <unordered_map>
#include <mutex>

typedef std::unordered_map<int, std::vector<TagType>> TagsMap;

int g_tag_list_count(0);
TagsMap g_tags;
std::mutex g_mutex;

extern "C"
{
    __declspec(dllexport) int __stdcall CreateNewTagList();
    __declspec(dllexport) void __stdcall ReleaseTagList(int tagLstNum);
    __declspec(dllexport) void __stdcall AddTag(int tagLstNum, char *key, char *value);
    __declspec(dllexport) bool __stdcall StampPDFFromXmlFile(char *srcFile, char *outFile, char *xmlFile, int tagLstNum);
    __declspec(dllexport) bool __stdcall StampPDFFromXml(char *srcFile, char *outFile, char *xml);
    __declspec(dllexport) bool __stdcall SaveSampleXml(char *outFile);
}

int __stdcall CreateNewTagList()
{
    std::lock_guard<std::mutex> lck(g_mutex);

    int max_limit = (std::numeric_limits<int>::max)() - 10;
    if (g_tag_list_count > max_limit)
        g_tag_list_count = 0;

    int num = ++g_tag_list_count;

    g_tags[num] = std::vector<TagType>();

    return num;
}

void __stdcall ReleaseTagList(int tagLstNum)
{
    std::lock_guard<std::mutex> lck(g_mutex);

    TagsMap::const_iterator it = g_tags.find(tagLstNum);
    if (it != g_tags.end())
    {
        g_tags.erase(it);
    }
}

void __stdcall AddTag(int tagLstNum, char *key, char *value)
{
    std::lock_guard<std::mutex> lck(g_mutex);

    TagsMap::iterator it = g_tags.find(tagLstNum);
    if (it != g_tags.end())
    {
        it->second.push_back(std::make_pair(key, value));
    }
}

bool __stdcall StampPDFFromXmlFile(char *srcFile, char *outFile, char *xmlFile, int tagLstNum)
{
    bool ok(false);
    std::vector<PrintData> vdt;

    if(tagLstNum)
    {
        {
            std::lock_guard<std::mutex> lck(g_mutex);

            TagsMap::iterator it = g_tags.find(tagLstNum);
            if (it != g_tags.end())
            {
                ok = PrintData::loadListFormXmlFile(vdt, xmlFile, it->second);
            }
        }

        ReleaseTagList(tagLstNum);
    }
    else
    {
        ok = PrintData::loadListFormXmlFile(vdt, xmlFile);
    }


    if(ok)
    {
        CStamper p(srcFile, outFile, vdt);
        ok = p.stampDocument();
    }

    return ok;
}

bool __stdcall StampPDFFromXml(char *srcFile, char *outFile, char *xml)
{
    bool ok(false);
    std::vector<PrintData> vdt;

    ok = PrintData::loadListFormXml(vdt, xml);

    if(ok)
    {
        CStamper p(srcFile, outFile, vdt);
        ok = p.stampDocument();
    }

    return ok;
}

bool __stdcall SaveSampleXml(char *outFile)
{
    return PrintData::saveXmlFileSample(outFile);
}
