#include "Stamper.h"
#include <iostream>

static void printUsage()
{
    std::cout << "Usage:\n"
                 "  -src: Original PDF file\n"
                 "  -dest: Output file\n"
                 "  -xml: Template XML file\n"
                 "\n"
                 "To add tags for replace use:\n"
                 "  <tag> <replace content>\n"
                 "\n"
                 "To generate a sample xml:\n"
                 "  -sample Sample file\n"
                 "\n"
                 "Example of use:\n"
                 "  -src \"C://MyFile.pdf\" -dest \"C://OutFile.pdf\" -xml \"C://MyTemplate.xml\" "
                 "\"[NUMBER]\" \"AB-1234\" \"__NAME__\" \"Mr. Bob\"\n";
}

int main(int argc, char* argv[])
{
    std::string src;
    std::string dest;
    std::string xmlFile;
    std::string sampleFile;
    std::vector<TagType> vRepTags;

    int currArg(2);
    while (currArg < argc)
    {
        std::string argKey(argv[currArg-1]);
        std::string argVal(argv[currArg]);

        if(argKey == "-src")
            src = argVal;
        else if(argKey == "-dest")
            dest = argVal;
        else if(argKey == "-xml")
            xmlFile = argVal;
        else if(argKey == "-sample")
            sampleFile = argVal;
        else
            vRepTags.push_back(std::make_pair(argKey, argVal));

        currArg += 2;
    }

    if(argc%2 == 0 || ((src.empty() || dest.empty() || xmlFile.empty()) && sampleFile.empty()))
    {
        std::cout << "Invalid arguments" << std::endl;
        printUsage();
        return -1;
    }

    if(!sampleFile.empty())
    {
        PrintData::saveXmlFileSample(sampleFile);
        return 0;
    }

     std::vector<PrintData> vdt;
     if(!PrintData::loadListFormXmlFile(vdt, xmlFile, vRepTags))
        return -1;

     CStamper p(src, dest, vdt);
     if(!p.stampDocument())
         return -1;

	return 0;
}

