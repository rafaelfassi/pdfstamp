#include "PrintData.h"
#include "tinyxml2.h"

#include <iostream>
#include <fstream>
#include <streambuf>

using namespace tinyxml2;

PrintData::PrintData() : type(PDT_TEXT), pageNum(0), pages(PDP_FIRST | PDP_MIDDLE | PDP_LAST),
    topLayer(true), transparent(false), fill(false), posX(-1.0), posY(-1.0), width(-1.0), height(-1.0),
    leftMargin(0.0), rightMargin(0.0), topMargin(0.0), bottomMargin(0.0), rotationDeg(0.0), borderWidth(1.0),
    align(PDA_H_CENTER | PDA_V_CENTER), fontSize(12), color(0x000000)
{
}

bool PrintData::loadListFormXmlFile(std::vector<PrintData> &vdt, const std::string &xmlFile,
                                    std::vector<TagType> &vtg)
{
    std::ifstream fl(xmlFile.c_str());
    std::string xml;

    if(fl.fail())
    {
        std::cout << "Failed to open xml file for: " << xmlFile << std::endl;
        return false;
    }

    fl.seekg(0, std::ios::end);
    xml.reserve(fl.tellg());
    fl.seekg(0, std::ios::beg);

    xml.assign((std::istreambuf_iterator<char>(fl)),
                std::istreambuf_iterator<char>());

    for(std::vector<TagType>::const_iterator it = vtg.begin(); it != vtg.end(); ++it)
    {
        for(std::string::size_type i = 0; (i = xml.find(it->first, i)) != std::string::npos;)
        {
            xml.replace(i, it->first.length(), it->second);
            i += it->second.length();
        }
    }

    return loadListFormXml(vdt, xml);
}

bool PrintData::loadListFormXml(std::vector<PrintData> &vdt, const std::string &xml)
{
    const std::string vtrue = "true";

    XMLDocument doc;
    if(doc.Parse(xml.c_str()) != XML_SUCCESS)
    {
        std::cout << "Error to parse xml file" << std::endl;
        return false;
    }

    XMLElement *root = doc.FirstChildElement("PrintDataList");
    if(!root)
    {
        std::cout << "Invalid xml content" << std::endl;
        return false;
    }

    XMLElement *listIt = root->FirstChildElement("PrintData");
    while (listIt)
    {
        PrintData dt;
        XMLElement *el;
        std::string tmp;

        el = listIt->FirstChildElement("type");
        if(el && el->GetText())
        {
            tmp = el->GetText();
            if(tmp == "TEXT")
                dt.type = PDT_TEXT;
            else if(tmp == "IMG")
                dt.type = PDT_IMG;
            else if(tmp == "RECT")
                dt.type = PDT_RECT;
        }

        el = listIt->FirstChildElement("pageNum");
        if(el && el->GetText())
            dt.pageNum = std::stoi(el->GetText());

        el = listIt->FirstChildElement("pages");
        if(el && el->GetText())
        {
            tmp = el->GetText();
            dt.pages = 0;
            if(tmp.find("FIRST") != std::string::npos)
                dt.pages |= PDP_FIRST;
            if(tmp.find("MIDDLE") != std::string::npos)
                dt.pages |= PDP_MIDDLE;
            if(tmp.find("LAST") != std::string::npos)
                dt.pages |= PDP_LAST;
            if(tmp.find("ODD") != std::string::npos)
                dt.pages |= PDP_ODD;
            if(tmp.find("EVEN") != std::string::npos)
                dt.pages |= PDP_EVEN;
        }

        el = listIt->FirstChildElement("topLayer");
        if(el && el->GetText())
            dt.topLayer = (el->GetText() == vtrue);

        el = listIt->FirstChildElement("transparent");
        if(el && el->GetText())
            dt.transparent = (el->GetText() == vtrue);

        el = listIt->FirstChildElement("fill");
        if(el && el->GetText())
            dt.fill = (el->GetText() == vtrue);

        el = listIt->FirstChildElement("posX");
        if(el && el->GetText())
            dt.posX = std::stod(el->GetText());

        el = listIt->FirstChildElement("posY");
        if(el && el->GetText())
            dt.posY = std::stod(el->GetText());

        el = listIt->FirstChildElement("width");
        if(el && el->GetText())
            dt.width = std::stod(el->GetText());

        el = listIt->FirstChildElement("height");
        if(el && el->GetText())
            dt.height = std::stod(el->GetText());

        el = listIt->FirstChildElement("leftMargin");
        if(el && el->GetText())
            dt.leftMargin = std::stod(el->GetText());

        el = listIt->FirstChildElement("rightMargin");
        if(el && el->GetText())
            dt.rightMargin = std::stod(el->GetText());

        el = listIt->FirstChildElement("topMargin");
        if(el && el->GetText())
            dt.topMargin = std::stod(el->GetText());

        el = listIt->FirstChildElement("bottomMargin");
        if(el && el->GetText())
            dt.bottomMargin = std::stod(el->GetText());

        el = listIt->FirstChildElement("rotationDeg");
        if(el && el->GetText())
            dt.rotationDeg = std::stod(el->GetText());

        el = listIt->FirstChildElement("borderWidth");
        if(el && el->GetText())
            dt.borderWidth = std::stod(el->GetText());

        el = listIt->FirstChildElement("align");
        if(el && el->GetText())
        {
            tmp = el->GetText();
            dt.align = 0;
            if(tmp.find("H_LEFT") != std::string::npos)
                dt.align |= PDA_H_LEFT;
            if(tmp.find("H_RIGHT") != std::string::npos)
                dt.align |= PDA_H_RIGHT;
            if(tmp.find("H_CENTER") != std::string::npos)
                dt.align |= PDA_H_CENTER;
            if(tmp.find("H_EXPAND") != std::string::npos)
                dt.align |= PDA_H_EXPAND;
            if(tmp.find("V_TOP") != std::string::npos)
                dt.align |= PDA_V_TOP;
            if(tmp.find("V_BOTTOM") != std::string::npos)
                dt.align |= PDA_V_BOTTOM;
            if(tmp.find("V_CENTER") != std::string::npos)
                dt.align |= PDA_V_CENTER;
            if(tmp.find("V_EXPAND") != std::string::npos)
                dt.align |= PDA_V_EXPAND;
        }

        el = listIt->FirstChildElement("fontSize");
        if(el && el->GetText())
            dt.fontSize = std::stoi(el->GetText());

        el = listIt->FirstChildElement("color");
        if(el && el->GetText())
            dt.color = std::stoi(el->GetText(), 0, 16);

        el = listIt->FirstChildElement("name");
        if(el && el->GetText())
            dt.name = el->GetText();

        el = listIt->FirstChildElement("parent");
        if(el && el->GetText())
            dt.parent = el->GetText();

        el = listIt->FirstChildElement("data");
        if(el && el->GetText())
            dt.data = el->GetText();

        el = listIt->FirstChildElement("font");
        if(el && el->GetText())
            dt.font = el->GetText();

        vdt.push_back(dt);
        listIt = listIt->NextSiblingElement("PrintData");
    }

    return true;
}


bool PrintData::saveXmlFileSample(const std::string &xmlFile)
{
    PrintData dt;
    XMLDocument doc;
    XMLElement *rootEl = doc.NewElement("PrintDataList");
    doc.InsertFirstChild(rootEl);

    XMLElement *itemEl = doc.NewElement("PrintData");
    rootEl->InsertEndChild(itemEl);

    XMLElement *el(0);
    XMLComment *cm(0);

    el = doc.NewElement("type");
    cm = doc.NewComment("Tipo de objeto a ser desenhado: TEXT=Texto; IMG=Imagem; RECT=Retangulo");
    el->SetText("TEXT");
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("name");
    cm = doc.NewComment("Nome do item. Usado para outro item referenciar como pai permitindo heranca");
    el->SetText(dt.name.c_str());
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("parent");
    cm = doc.NewComment("Pai do item. Referencia o nome do objeto pai");
    el->SetText(dt.parent.c_str());
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("pageNum");
    cm = doc.NewComment("Desenha o item em uma pagina espacifica");
    el->SetText((int)dt.pageNum);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("pages");
    cm = doc.NewComment("Paginas que o item deve ser desenhado: FIRST|MIDDLE|LAST|ODD|EVEN");
    el->SetText("FIRST|MIDDLE|LAST");
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("topLayer");
    cm = doc.NewComment("Desenha o item sobre do conteudo original da pagina");
    el->SetText(dt.topLayer);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("transparent");
    cm = doc.NewComment("Desenha o item transparente. Nao funciona para imagens, nas quais a transparencia pode ser adicionada no proprio arquivo png");
    el->SetText(dt.transparent);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("rotationDeg");
    cm = doc.NewComment("Rotacao do item em graus");
    el->SetText(dt.rotationDeg);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("borderWidth");
    cm = doc.NewComment("Tamanho da borda para o elemento retangulo.");
    el->SetText(dt.borderWidth);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("fill");
    cm = doc.NewComment("Quando o desenho for do tipo retangulo, indica se o mesmo deve ser preenchido");
    el->SetText(dt.fill);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("posX");
    cm = doc.NewComment("Forcar uma posicao absoluta na horizontal");
    el->SetText(dt.posX);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("posY");
    cm = doc.NewComment("Forcar uma posicao absoluta na vertical");
    el->SetText(dt.posY);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("width");
    cm = doc.NewComment("Largura do item. Para imagem e texto este parametro e opcional");
    el->SetText(dt.width);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("height");
    cm = doc.NewComment("Altura do item. Para imagem e texto este parametro e opcional");
    el->SetText(dt.height);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("align");
    cm = doc.NewComment("Alinhamentos do item: H_LEFT|H_RIGHT|H_CENTER|H_EXPAND|V_TOP|V_BOTTOM|V_CENTER|V_EXPAND");
    el->SetText("H_CENTER|V_CENTER");
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("leftMargin");
    cm = doc.NewComment("Tamanho da margem esquerda quando se usa alinhamento");
    el->SetText(dt.leftMargin);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("rightMargin");
    cm = doc.NewComment("Tamanho da margem direita quando se usa alinhamento");
    el->SetText(dt.rightMargin);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("topMargin");
    cm = doc.NewComment("Tamanho da margem superior quando se usa alinhamento");
    el->SetText(dt.topMargin);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("bottomMargin");
    cm = doc.NewComment("Tamanho da margem inferior quando se usa alinhamento");
    el->SetText(dt.bottomMargin);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("color");
    cm = doc.NewComment("Codigo RGB da cor para item de texto e retangulo");
    el->SetText("000000");
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("font");
    cm = doc.NewComment("Caminho para o arquivo de fonte a ser usado quanto o item for do tipo texto");
    el->SetText(dt.font.c_str());
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("fontSize");
    cm = doc.NewComment("Tamanho da fonte para texto");
    el->SetText(dt.fontSize);
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    el = doc.NewElement("data");
    cm = doc.NewComment("Quando o item for imagem este parametro indica o caminho da mesma e quando for texto indica o texto a ser desenhado");
    el->SetText(dt.parent.c_str());
    itemEl->InsertEndChild(cm);
    itemEl->InsertEndChild(el);

    doc.InsertFirstChild(doc.NewDeclaration());

    if(doc.SaveFile(xmlFile.c_str()) != XML_SUCCESS)
    {
        std::cout << "Error to save xml file" << std::endl;
        return false;
    }

    return true;
}
