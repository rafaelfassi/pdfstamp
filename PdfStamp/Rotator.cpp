#include "Rotator.h"
#include "PageContentContext.h"
#include <math.h>

#define VAL_PI 3.14159265358979323846

Rotator::Rotator() : offsetX(0.0), offsetY(0.0), width(0.0), height(0.0), cosVal(1.0), sinVal(0.0)
{
}

void Rotator::process(double rotation, double orWidth, double orHeight)
{
    double deg = std::abs(rotation);
    if(deg > 90)
        deg = 90 + 90 - deg;

    double rad = std::abs(deg*VAL_PI/180);
    double cosP = std::cos(rad);
    double sinP = std::sin(rad);

    cosVal = std::cos(rotation*VAL_PI/180);
    sinVal = std::sin(rotation*VAL_PI/180);

    width = cosP*orWidth + sinP*orHeight;
    height = sinP*orWidth + cosP*orHeight;

    if(rotation > 0.0 && rotation <= 90.0)
    {
        offsetX = sinP*orHeight;
        offsetY = 0;
    }
    else if(rotation > 90.0 && rotation <= 180.0)
    {
        offsetX = width;
        offsetY = cosP*orHeight;
    }
    else if(rotation > 180)
    {
        offsetX = width - sinP*orHeight;
        offsetY = height;
    }
    else if(rotation < 0.0 && rotation >= -90.0)
    {
        offsetX = 0;
        offsetY = height - cosP*orHeight;
    }
    else if(rotation < -90.0 && rotation >= -180.0)
    {
        offsetX = width - sinP*orHeight;
        offsetY = height;
    }
    else if(rotation < -180)
    {
        offsetX = width;
        offsetY = cosP*orHeight;
    }
}

void Rotator::rotate(PageContentContext* pageContentContext, double posX, double posY)
{
    //pageContentContext->cm(cos q, sin q, -sin q, cos q, 0, 0)
    pageContentContext->cm(cosVal, sinVal, sinVal*-1.0, cosVal, posX, posY);
}
